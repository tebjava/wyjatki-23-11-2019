/*
 * Zadanie:
 * - poprzednio tworzony kalkulator wzbogacic o obsluge wyjatkow/bledow
 * - dodac funkcje, ktore beda mialy za zadanie wczytywac wartosci do kalkulatora 
 * na wzor kodu ponizej (czyli wczytujaca jedynie wartosci typu float i zadne inne)
 * - przy dzieleniu takze zastapic potencjalny if na try - catch
 * - dodatkowo w przypadku wyjatku dzielenia przez zero sprawdzic funkcjonowanie klauzuli
 * finally; przyklad:
 * try {
 * 		//kod
 * }
 * catch (Exception e) {
 * 		//obsluga bledu, np. jakis komunikat
 * }
 * finally {
 * 	    //kod - wlasnie, jak on sie wykona?
 * }
 * 
 * ZADANIE 2:
 * - przetowrzyc zmienne na float
 * - doprowadzic do momentu by menu dzialalo do chwili az uzytkownik nie wpisze liczby zaknaczajacej
 * dzialanie programu (mozna wykorzystac rekurencje)
 * - stworzyc potegowanie za pomoca rekurencji
 * - zastanowic sie (i najlepiej wykonac) w jaki sposob wysietlic uzytkownik wszystkie podane
 * przez niego liczby, z ktorych wyswietlamy mu wynik (np. wyswietlic mu 10 liczb ktore wprowadzil)
 * - obsluzyc wszelkie bledy wprowadzania (np. podales zero albo podales wartosc ujemna - szczegolnie
 * tyczy sie silni)
 * - zobaczyc czym jest i wprowadzic modulo (i sprawdzic czy/czemu nie dziala na zmiennoprzecinkowych).
 */

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Kalkulator {
	
	static int wczytajInt() {
		@SuppressWarnings("resource")
		var scan = new Scanner(System.in);
		try {
			return Integer.parseInt(scan.next());
		}
		catch (Exception e) {
			System.err.println("Wpisałes wartosc inna niz int!");
		}
		return 0;
	}
	
	static int wczytajUInt() {
		@SuppressWarnings("resource")
		var scan = new Scanner(System.in);
		//String w = scan.next();
		int ret = -1;
		try {
			ret = Integer.parseInt(scan.next());
			//to informacja, nie blad!
			if (ret<0) println("Podana wartosc jest mniejsza od zera!");
			return ret;
		}
		catch (Exception e) {
			System.err.println("Wpisałes wartosc inna niz int!");
		}
		return -1;
	}
	
	static void print(Object o) {
		System.out.print(o);
	}
	
	static void println(Object o) {
		System.out.println(o);
	}
	
	static void menu() {
		System.out.println("Kalkulator w Java");
		System.out.println("Wybierz jedna z operacji:");
		System.out.println("1. Dodawanie\n2. Odejmowanie"
				+ "\n3. Mnożenie\n4. Dzielenie"
				+ "\n5. Potegowanie\n6. Pierwiastkowanie"
				+ "\n7. Silnia\n8. Wyjscie");
		System.out.print("Wybor: ");

		int wybor;
		do {
			wybor=wczytajUInt();
		} while(wybor<0);
		int ilosc=0;
		print("Podaj na ilu wartosciach chcesz dokonac wybranej operacji: ");
		ilosc = wczytajUInt();
	    switch(wybor) {
		    case 1: dodawanie(ilosc); break;
		    case 2: odejmowanie(ilosc); break;
		    case 3: mnozenie(ilosc); break;
		    case 4: dzielenie(ilosc); break;
		    case 5: potegowanie(ilosc); break;
		    case 6: pierwiastkowanie(ilosc); break;
		    case 7: silnia(ilosc); break;
	    }
	}
	
	static void dodawanie(int ilosc) {
		int suma=0,wczytana;
		//ilosc++ -> ilosc=ilosc+1;
		for (int i=0;i<ilosc;i++) {
			/*
			 * Ponizszy kod ma tzw. zlozonosc kwadratowa. Kod tego typu to przewaznie 
			 * wykonywanie zagniezdzonych petli (petla glowna, podpetla/podtle). Tego typu
			 * rozwiazania sa "najciezsze" dla komputera i wykonuja sie przewaznie
			 * dwukrotnie wolniej niz kod tzw. liniowy (czyli posiadajacy jedna petle)
			 * Ponizsza zagniezdzona petla nie czyni kodu stricte kawadratowym bo jej zadaniem
			 * jest wymusic na uzytkowniku wpisania wartosci roznej od zera; jezeli od razu
			 * taka poda to funkcja zachowa sie liniowo
			 */
			do 
				wczytana=wczytajInt(); 
			while(wczytana==0);
			suma+=wczytana;
		}
		println("Suma podanych liczb to: " + suma);
	}
	
	static void odejmowanie(int ilosc) {
		
	}
	
	static void mnozenie(int ilosc) {
		int wynik=1;
		//1. Sprawdz czy ilosc jest wieksze od zera; jezeli jest wykonac kod while i zmniejsz 
		//ilosc o 1
		while(ilosc-->0) {
			//2. zmienna tymczasowa; twrzona KAZDORAZOWO przy kolejnym dzialaniu petli (np. 500 wywolan
			//to 500 razy utworzona zmienna tymczasowa -> nie jest to do konca efektywne
			int tymczasowa;
			//3. w kodzie Java czesto mozna sptkac tego typu rozwiazanie; w instrukcji if
			//MOZEMY przypisywac wartosci do zmiennych by nastepnie te wartosci (juz przypisanych
			//do zmiennej) porwnywac (poddawac operacjom porownan logicznych); oszczedza to
			//linjke kodu i dla niektorych jest czytelniejsze (nie dla wszystkich)
			if((tymczasowa=wczytajInt())==0)
			//4. jezeli zmienna o nazwie tymczasowa zawiera wartosc 0 - podnosimy ilosc 
			// (sterowanie iloscia wykonania petli) o 1, co znaczy, ze uzytkownik musi jeszcze raz
			//powtorzyc wprowadzanie liczby
				ilosc++;
			else
			//5. uzytkownik wprowadzil liczbe inna od 0; program wykonuje sie dalej bez bledow
				wynik*=tymczasowa;
		}
		//powyzsze rozwiazanie jest czysto liniowe; zawiera tylko jedna petle, ktora wykonuje sie
		//liniowo (wieksza wartosc ilosc -> dluzej, mniejsza wartosc -> krocej).
		println("Wynik mnozenia podanych liczb to: " + wynik);
	}
	
	static void dzielenie(int ilosc) {
		
	}
	
	static void potegowanie(int ilosc) {
		
	}
	
	static void pierwiastkowanie(int ilosc) {
		
	}
	
	//funkcja przeciazajaca; to te funkcje wywoluje uzytkownik kodu (wpisuje z jakiej wartosci
	//bedzie liczona silnia); drugi parametr dolozony zostal w postaci literalu i wynosi 1;
	//dzieki temu funkcja poprawnie liczy wartosc silni (jezeli uzytkownik poda wartosc 0 - wynosi 1;
	//mnozenie 1*1 tez daje jeden; kazde pozostale da prawidlowy wynik)
	static int silnia(int ilosc) {
		return silnia(ilosc, 1);
	}
	
	//rekurencja czyli wywolywanie funkcji przez sama siebie; w ponizszym wypadku nasza silnia
	//musi zapamietywac swoj wynik - stworzylismy zmienna pomocnicza wynik; uzytkownik docelowy
	//funkcji nie powinien nim jednak zaprzatac sobie glowy (bez sensu by przy kazdym wywolaniu
	//funkcji wpisywal w drugi paramatr wartosc 1) Stad wykorzystane zostalo takze przeciazenie
	//funkcji (funkcja napisana powyzej)
	static int silnia(int ilosc, int wynik) {
		if (ilosc==0) return wynik;
		return silnia(ilosc-1,wynik*ilosc);	
	}
	
	public static void main(String[] args) {
		println(silnia(3));
		//menu();
		
	}
	/*
	 * Ponizej opis dzialania inkrementacji jezykowej
	 * int zmienna=1;
		//zmienna++ tzw. inkrementacja czyli zwiekszanie wartosci liczbowej o 1
		//wystepuja dwa rodzaje:
		//- postinkremetacja; nastepuje PO wykonaniu biezacej operacji. Ponizej biezaca operacja
		//jest wyswietlenie wartosci zmiennej zmienna; wyswietli sie nam wartosc 1 (nie 2)
		println("ZMIENNA " + (zmienna++));
		//ale ponizej zmienna bedzie juz miala wartosc 2
		println("ZMIENNA " + zmienna);
		//- preinkrementacja; nastepuje PRZED wykonaniuem biezacej operacji. Ponizej biezaca
		//operacja jest wyswietlenie wartosci zmiennej zmienna; wyswietli sie nam wartosc 3 (nie 2)
		println("ZMIENNA " + (++zmienna));
		//ponizej nadal bedzie to wartosc 3 (nic sie juz z ta zmienna nie dzieje)
		println("ZMIENNA " + zmienna);
		
	  * ISTNIEJE TAKZE DEKREMENTACJA (obnizanie wartosci o 1) i dziala analogicznie
	 */
}
